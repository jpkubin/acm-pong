import pygame

WINDOW_WIDTH = 800
WINDOW_HEIGHT = 600
FPS = 30

BLACK = 0, 0, 0

SCORE_EVENT = pygame.event.custom_type()

pygame.init()
pygame.display.set_caption('Pong')
ice_cream_image = pygame.image.load('./icecream.png')
pygame.display.set_icon(ice_cream_image)
window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
clock = pygame.time.Clock()

background = pygame.image.load('./stone.jpg')
background = pygame.transform.scale(background, (WINDOW_WIDTH, WINDOW_HEIGHT))

arial = pygame.font.SysFont('arial', 48)

class Paddle(pygame.sprite.Sprite):
    def __init__(self, player: int, edge: int, *groups: pygame.sprite.AbstractGroup) -> None:
        super().__init__(*groups)
        self.player = player
        self.image = pygame.image.load('./scoop.png')
        height = WINDOW_HEIGHT / 5
        width = (height * self.image.get_width()) / self.image.get_height()
        self.image = pygame.transform.scale(self.image, (width, height))
        self.rect = self.image.get_rect()
        self.rect.centery = WINDOW_HEIGHT / 2

        if self.player == 2:
            self.rect.left = edge
            self.up = pygame.K_UP
            self.down = pygame.K_DOWN
        else:
            self.rect.right = edge
            self.image = pygame.transform.flip(self.image, True, False)
            self.up = pygame.K_w
            self.down = pygame.K_s

        self.mask = pygame.mask.from_surface(self.image)

    def update(self):
        up_pressed = self.up in keys_down
        down_pressed = self.down in keys_down

        if up_pressed and not down_pressed:
            if self.rect.top > 0:
                self.rect.top -= 5
        elif down_pressed and not up_pressed:
            if self.rect.bottom < WINDOW_HEIGHT:
                self.rect.bottom += 5


class Ball(pygame.sprite.Sprite):
    ROT_FRAMES = 360 // 5

    def __init__(self, *groups: pygame.sprite.AbstractGroup) -> None:
        super().__init__(*groups)
        self.base_image = ice_cream_image

        height = WINDOW_HEIGHT / 8
        width = (height * self.base_image.get_width()) / \
            self.base_image.get_height()
        self.base_image = pygame.transform.scale(
            self.base_image, (width, height))

        increment_angle = 360 // self.ROT_FRAMES

        self.frames = []
        for i in range(self.ROT_FRAMES):
            image = pygame.transform.rotate(
                self.base_image, i * increment_angle)
            mask = pygame.mask.from_surface(image)
            rect = image.get_rect()
            self.frames.append((image, mask, rect))

        self.center = WINDOW_WIDTH // 2, WINDOW_HEIGHT // 2

        self.frame = 0
        self.vx = 5
        self.vy = 5

        self.radius = round(height * 0.4)

        self.update()

    def reset(self):
        self.center = WINDOW_WIDTH // 2, WINDOW_HEIGHT // 2
        self.update()

    def update(self) -> None:
        super().update()
        self.frame += 1
        if self.frame >= self.ROT_FRAMES:
            self.frame -= self.ROT_FRAMES
        image, mask, rect = self.frames[self.frame]

        self.image = image
        self.mask = mask
        self.rect = rect
        self.rect.center = self.center

        if self.rect.centery - self.radius <= 0:
            self.vy *= -1
            self.centery = self.radius + 5
        elif self.rect.centery + self.radius >= WINDOW_HEIGHT:
            self.vy *= -1
            self.centery = WINDOW_HEIGHT - self.radius - 5

        if self.rect.centerx <= WINDOW_WIDTH * 0.1:
            pygame.event.post(
                pygame.event.Event(SCORE_EVENT, {
                    'player': 1
                })
            )
        elif self.rect.centerx >= WINDOW_WIDTH * 0.9:
            pygame.event.post(
                pygame.event.Event(SCORE_EVENT, {
                    'player': 2
                })
            )

        for paddle in pygame.sprite.spritecollide(self, paddles, False, pygame.sprite.collide_mask):
            self.vx *= -1
            self.rect.centerx += self.vx

        self.rect.centerx += self.vx
        self.rect.centery += self.vy

        self.center = self.rect.center


class Score(pygame.sprite.Sprite):
    def __init__(self, player: int, *groups: pygame.sprite.AbstractGroup) -> None:
        super().__init__(*groups)
        self.player = player
        self.score = 0

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, score):
        self._score = score
        self.image = arial.render(str(score), True, BLACK)
        self.rect = self.image.get_rect()
        if self.player == 2:
            self.rect.topright = (WINDOW_WIDTH - 30, 30)
        else:
            self.rect.topleft = (30, 30)


class GameText(pygame.sprite.Sprite):
    def __init__(self, text: str, *groups: pygame.sprite.AbstractGroup) -> None:
        super().__init__(*groups)
        self.text = text

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, text):
        self._text = 0
        self.image = arial.render(str(text), True, BLACK)
        self.rect = self.image.get_rect()
        self.rect.center = (WINDOW_WIDTH // 2, WINDOW_HEIGHT // 2)
    


ui = pygame.sprite.Group()
game_objects = pygame.sprite.Group()
paddles = pygame.sprite.Group()
left_paddle = Paddle(1, WINDOW_WIDTH * 0.1, game_objects, paddles)
right_paddle = Paddle(2, WINDOW_WIDTH * 0.9, game_objects, paddles)
p1_score = Score(1, ui)
p2_score = Score(2, ui)
game_text = GameText('Press any key...', ui)
ball = Ball(game_objects)

keys_down = set()

running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            keys_down.add(event.key)
        elif event.type == pygame.KEYUP:
            if game_text.alive():
                game_text.kill()
            keys_down.remove(event.key)
        elif event.type == SCORE_EVENT:
            if event.player == 1:
                winner = p1_score
            else:
                winner = p2_score

            winner.score = winner.score + 1

            if winner.score > 2:
                p1_score.score = 0
                p2_score.score = 0
                game_text.text = f'Player {winner.player} has won!'
            else:
                game_text.text = f'Press any key...'

            ball.reset()
            ui.add(game_text)

    if not game_text.alive():
        game_objects.update()
    ui.update()

    window.blit(background, (0, 0))
    game_objects.draw(window)
    ui.draw(window)
    pygame.display.update()

    clock.tick(FPS)

pygame.quit()
