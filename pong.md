---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url("https://marp.app/assets/hero-background.svg")
---

# **Python Pong**
#### Intro to Pygame and the Event Loop
### W&M ACM

#### 03/02/2023

---

# **Road Map**

1. PyGame Workshop (~6:30)
2. Pong Tournament (~7:00)
3. Technical Interview Prep Info Session (~7:20)

---

# **Let's Install PyGame**

Go ahead and run this command to get pygame installed on your machine. While this loads, we'll talk about what pygame actually is (always install things before you know what they are)

```sh
python -m pip install -U pygame --user
```

**Mac Users:** Use `python3` instead of `python`

**Don't have python on your computer?**

We will use Replit.com today! Go ahead and get signed in with a compatible account.

---

# **What is PyGame**

https://www.pygame.org/wiki/about

* Python library designed for making video games
* Free and open source
* Not opinionated (you have to choose how to structure your code)

---

# **What is PyGame**

https://www.pygame.org/wiki/about

* Python library designed for making video games
* Free and open source
* Not opinionated (you have to choose how to structure your code)


